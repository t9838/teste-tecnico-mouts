import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteNamesEnum } from './route-names.enum';
import { CooperatedModule } from './cooperated';

const routes: Routes = [
  {
    path: RouteNamesEnum.COOPERATED,
    loadChildren: () => CooperatedModule
  },
  {
    path: '**',
    redirectTo: RouteNamesEnum.COOPERATED
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
