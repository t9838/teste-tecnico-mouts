import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterceptorsModule } from '../../interceptors';
import { MaterialModule } from '../../material';
import { PlataformModule } from '../../plataform';
import { LayoutModule } from '../../layout';
import { ToastModule } from '../../toast';

const Modules = [
  InterceptorsModule,
  MaterialModule,
  PlataformModule,
  LayoutModule,
  ToastModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...Modules
  ],
  exports: [
    ...Modules
  ]
})
export class CoreModule { }
