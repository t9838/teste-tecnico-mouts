export const SameChar = (str: string): boolean => {
  if(!str){
    return false
  }
  const FirstChar = str.slice(0, 1);
  let sameChar = true;

  Array.from(str).forEach((char) => {
    if (char !== FirstChar) {
      sameChar = false;
    }
  });

  return sameChar;
};

export const validateCPF = (cpf: string) : boolean => {
  let Soma;
  let Resto;
  Soma = 0;

  if(!cpf){
    return false
  }

  if (SameChar(cpf) || cpf.length !== 11) {
    return false;
  }

  for (let i = 1; i <= 9; i++) {

    Soma = Soma + parseInt(cpf.substring(i - 1, i), 10) * (11 - i);

  }

  Resto = (Soma * 10) % 11;

  if ((Resto === 10) || (Resto === 11)) {
    Resto = 0;
  }
  if (Resto !== parseInt(cpf.substring(9, 10), 10)) {

    return false;

  }

  Soma = 0;
  for (let i = 1; i <= 10; i++) {

    Soma = Soma + parseInt(cpf.substring(i - 1, i), 10) * (12 - i);

  }
  Resto = (Soma * 10) % 11;

  if ((Resto === 10) || (Resto === 11)) {
    Resto = 0;
  }
  if (Resto !== parseInt(cpf.substring(10, 11), 10)) {

    return false;

  }

  return true;
}

export const validateSameString = (strA: string, strB: string) : boolean => {

  return strA === strB
}
