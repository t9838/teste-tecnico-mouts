export enum SideNavModes {
  SIDE = 'side',
  OVER = 'over',
  PUSH = 'push'
}
