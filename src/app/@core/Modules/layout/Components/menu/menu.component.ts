import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { LayoutService } from '../../Services';
import { MenuItemDTO } from '../../DTO';
import { Subscription } from 'rxjs';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {

  @Output() public handleRedirect: EventEmitter<MenuItemDTO> = new EventEmitter()

  private subscriptions: Subscription[] = [];

  public menuItens: MenuItemDTO[] = [];
  public activeModule: string = '';

  constructor(
    private readonly layoutService: LayoutService,
    private readonly router: Router
  ) { }


  ngOnInit(): void {
    this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      if(!subscription.closed){
        subscription.unsubscribe();
      }
    })
  }

  private initSubscriptions(): void{
    const MenuSubscription = this.layoutService.getMenuItens().subscribe(res => {
      this.menuItens = res;
    });

    this.subscriptions.push(MenuSubscription);

    const RouterSubscription = this.router.events.subscribe(res => {
      if(res instanceof NavigationStart){
        const Module = res.url.split('/')[1];
        this.activeModule = Module;
      }
    })

    this.subscriptions.push(RouterSubscription);
  }

  public redirect(target:MenuItemDTO ): void{

    this.handleRedirect.emit(target);

    if(target.module){
      this.activeModule = target.module;
    }
  }

}
