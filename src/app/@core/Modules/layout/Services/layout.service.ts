import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { MenuItemDTO } from '../DTO'

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  private menuItens: ReplaySubject<MenuItemDTO[]> = new ReplaySubject<MenuItemDTO[]>()

  constructor() {
    this.loadMenuItens();
  }

  private loadMenuItens(){
    const MenuItens = [
      {
        label: 'Sample 1',
        icon: 'menu'
      },
      {
        label: 'Admissão',
        icon: 'search',
        route: 'cooperated',
        module: 'cooperated'
      },
      {
        label: 'Sample 2',
        icon: 'star_border'
      },
      {
        label: 'Sample 3',
        icon: 'message'
      },
      {
        label: 'Sample 4',
        icon: 'tune'
      },
      {
        label: 'Sample 5',
        icon: 'account_balance'
      }
    ];

    this.setMenuItens(MenuItens);
  }

  private setMenuItens(menuList: MenuItemDTO[]): void{
    this.menuItens.next(menuList);
  }

  public getMenuItens():Observable<MenuItemDTO[]>{
    return this.menuItens.asObservable()
  }

}
