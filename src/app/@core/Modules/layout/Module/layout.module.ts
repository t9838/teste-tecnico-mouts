import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from '../Components';
import { LayoutService } from '../Services';

import { MatIconModule } from '@angular/material/icon';

const Material = [
  MatIconModule
]

const Components = [
  MenuComponent
]

const Providers = [
  LayoutService
]

@NgModule({
  declarations: [
    ...Components
  ],
  imports: [
    CommonModule,
    ...Material
  ],
  providers: [
    ...Providers
  ],
  exports: [
    ...Components
  ]
})
export class LayoutModule { }
