export interface MenuItemDTO {
  label: string;
  icon: string;
  route?: string;
  module?: string;
}
