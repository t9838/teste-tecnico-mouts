import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlataformService } from '../Service';

const Providers = [
  PlataformService
]

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ...Providers
  ]
})
export class PlataformModule { }
