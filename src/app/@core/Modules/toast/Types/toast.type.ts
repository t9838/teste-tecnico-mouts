import { ToastEnum } from '../Enums';

export declare type ToastType = ToastEnum.SUCCESS | ToastEnum.DANGER | ToastEnum.WARNING
