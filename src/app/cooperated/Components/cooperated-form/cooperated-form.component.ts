import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@ngneat/reactive-forms';
import {
  AbstractControl,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { CooperatedDTO } from '../../DTO';
import { CardTemplateEnum } from '../../Enums';
import { CooperatedService } from '../../Service';
import { CPFMask, validateCPF } from '../../../@core';

@Component({
  selector: 'app-cooperated-form',
  templateUrl: './cooperated-form.component.html',
  styleUrls: ['./cooperated-form.component.scss']
})
export class CooperatedFormComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];

  public cooperatedForm!: FormGroup<CooperatedDTO>;
  public CPFMask = CPFMask;
  public foundCPF: boolean =  false;
  public CardTemplateEnum = CardTemplateEnum;
  public headerHTMl = `
    <h1>NOVA ADMISSÃO COOPERADO</h1>
    <span>Cadastro / Admissão do Cooperado / Nova Admissão de Cooperado</span>
  `

  constructor(
    private readonly cooperatedService: CooperatedService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      if(!subscription.closed){
        subscription.unsubscribe();
      }
    })
  }

  private initForm(): void{
    this.cooperatedForm = this.formBuilder.group({
      cpf: ['', [Validators.required, this.validarCPF]],
      name: null,
      status: null,
      applicationAccount: null,
      checkingAccount: null
    })
  }

  private validarCPF = (control: AbstractControl): ValidationErrors | null => {

    if (!validateCPF(control.value)) {
      const Response: ValidationErrors = {
        confirm: true,
        error: true
      }
      return Response;
    }

    return null

  };

  public searchCPF(){
    const CPF = this.cooperatedForm.controls.cpf.value;

    if(CPF && this.cooperatedForm.controls.cpf.valid){
      this.cooperatedService.getCooperatedByCPF(CPF).subscribe(res => {
        this.cooperatedForm.patchValue({ ...res });
        this.foundCPF = true;
      })
    }
  }

  public resetForm(): void {
    this.cooperatedForm.reset();
    this.foundCPF = false;
  }

  public getCooperatedFromForm(): CooperatedDTO{
    const CooperatedFromForm = this.cooperatedForm.value;
    return CooperatedFromForm
  }

}
