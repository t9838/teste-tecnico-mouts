import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CooperatedFormComponent } from './cooperated-form.component';

describe('CooperatedFormComponent', () => {
  let component: CooperatedFormComponent;
  let fixture: ComponentFixture<CooperatedFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CooperatedFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperatedFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
