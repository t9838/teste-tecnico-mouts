import { Component, OnInit, Input } from '@angular/core';
import { CooperatedDTO} from '../../DTO';
import { CardTemplateType } from '../../Types';
import { CardTemplateEnum } from '../../Enums';
import {
  AccountMask
} from '../../../@core';

@Component({
  selector: 'app-cooperated-card',
  templateUrl: './cooperated-card.component.html',
  styleUrls: ['./cooperated-card.component.scss']
})
export class CooperatedCardComponent implements OnInit {

  @Input() public title!: string;
  @Input() public subtitle!: string;
  @Input() public cooperated!: CooperatedDTO;
  @Input() public template!: CardTemplateType;

  public CardTemplateEnum = CardTemplateEnum
  public AccountMask = AccountMask;

  constructor() { }

  ngOnInit(): void {
  }

}
