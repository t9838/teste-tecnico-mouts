import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CooperatedCardComponent } from './cooperated-card.component';

describe('CooperatedCardComponent', () => {
  let component: CooperatedCardComponent;
  let fixture: ComponentFixture<CooperatedCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CooperatedCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperatedCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
