export interface CooperatedDTO {
  cpf: string;
  name: string | null;
  status: string | null;
  applicationAccount: string | null;
  checkingAccount: string | null;
}
