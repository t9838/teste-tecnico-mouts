import { TestBed } from '@angular/core/testing';

import { CooperatedService } from './cooperated.service';

describe('CooperatedService', () => {
  let service: CooperatedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CooperatedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
