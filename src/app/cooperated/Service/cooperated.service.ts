import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, ReplaySubject } from 'rxjs';
import { CooperatedDTO } from '../DTO';
import { ToastService, ToastEnum } from '../../@core';

const {
  API_URL,
} = environment;

@Injectable({
  providedIn: 'root'
})
export class CooperatedService {

  constructor(
    private readonly httpClient: HttpClient,
    private readonly toastService: ToastService
  ){}

  public getCooperatedByCPF(cpf: string): Observable<CooperatedDTO>{
    const APIResponse = new ReplaySubject<CooperatedDTO>();

    this.httpClient.get<CooperatedDTO[]>(`${API_URL}/cooperated?cpf=${cpf}`).subscribe(res => {
      if(res.length){
        APIResponse.next(res[0]);
      }
      this.toastService.showToast({
        data: {
          content: 'CPF não encontrado'
        },
        type: ToastEnum.DANGER
      })
    }, err => {
      this.toastService.showToast({
        data: {
          content: err.message
        },
        type: ToastEnum.DANGER
      })
      APIResponse.error(err);
    })

    return APIResponse.asObservable();
  }
}
