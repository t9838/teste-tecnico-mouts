export enum CardTemplateEnum {
  STATUS = 'status',
  APPLICATION_ACCOUNT = 'applicationAccount',
  CHECKING_ACCOUNT = 'checkingAccount'
}
