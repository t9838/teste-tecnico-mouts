import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaskModule } from 'ngx-mask'

import { CooperatedFormComponent, CooperatedCardComponent } from '../Components';
import { CooperatedService } from '../Service';
import { CooperatedRoutes } from '../Routes';

import { CoreModule } from '../../@core';


const Components = [
  CooperatedFormComponent,
  CooperatedCardComponent
]

const Modules = [
  RouterModule.forChild(CooperatedRoutes),
  HttpClientModule,
  CoreModule,
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  NgxMaskModule.forRoot()
]

const Providers = [
  CooperatedService
]

@NgModule({
  declarations: [
    ...Components
  ],
  imports: [
    CommonModule,
    ...Modules
  ],
  providers: [
    ...Providers
  ]
})
export class CooperatedModule { }
