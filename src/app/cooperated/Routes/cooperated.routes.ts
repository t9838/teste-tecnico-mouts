import { Routes } from '@angular/router';
import { CooperatedFormComponent } from '../Components';
import { CooperatedRouteNamesEnum } from './cooperated-route-names.enum';

export const CooperatedRoutes: Routes = [
  {
    path: CooperatedRouteNamesEnum.FORM,
    component: CooperatedFormComponent
  },
  {
    path: '**',
    redirectTo: CooperatedRouteNamesEnum.FORM
  }
]
