import { CardTemplateEnum } from '../Enums';

export declare type CardTemplateType = CardTemplateEnum.STATUS | CardTemplateEnum.APPLICATION_ACCOUNT | CardTemplateEnum.CHECKING_ACCOUNT
