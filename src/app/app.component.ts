import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDrawerMode } from '@angular/material/sidenav';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  SideNavModes,
  PlataformService,
  PlataformEnum,
  MenuItemDTO
} from './@core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{

  private subscriptions: Subscription[] = [];
  public sideNavMode: MatDrawerMode = SideNavModes.SIDE;
  public currentPlataform!: string;
  public PlataformEnum = PlataformEnum;
  public openSideNav!: boolean;
  public headerHTMl !: string;

  constructor(
    private readonly plataformService: PlataformService,
    private readonly iconRegistry: MatIconRegistry,
    private readonly sanitizer: DomSanitizer,
    private readonly router: Router

  ){
    this.loadCustomSVG()

  }

  ngOnInit(): void {
    this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      if(!subscription.closed){
        subscription.unsubscribe();
      }
    })
  }

  private initSubscriptions(): void{

    const PlataformSubscription = this.plataformService.getPlataform().subscribe(res => {
      this.currentPlataform = res;

      if(res === PlataformEnum.MOBILE){
        this.sideNavMode = SideNavModes.OVER;
        this.setSideNavOpenState(false);
      }
      else{
        this.sideNavMode = SideNavModes.SIDE;
        this.setSideNavOpenState(true);
      }
    })

    this.subscriptions.push(PlataformSubscription);
  }

  private loadCustomSVG(): void{
    this.iconRegistry.addSvgIcon(
      'logo',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/images/logo.svg')
    );
  }

  public setSideNavOpenState(isOpen: boolean): void{
    this.openSideNav = isOpen;
  }

  public redirect(target:MenuItemDTO ): void{

    if(target.route){
      this.router.navigate([target.route]);
    }

    if(this.currentPlataform === PlataformEnum.MOBILE){
      this.setSideNavOpenState(false);
    }

  }

  public handleOutletLoaded(component: any): void {
    this.headerHTMl = component?.['headerHTMl'];
  }
}
