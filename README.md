## Descrição

Frontend para a visualização de um cooperado, realizado utilizando a framework [Angular 13](https://angular.io/) em conjunto com biblioteca de componentes [Angular Material](https://material.angular.io/)

## Instalação

```bash
$ npm install
```

## Executando a aplicação

```bash

# Configura o mock do banco de dados
$ npm run server

# Inicia a aplicação
$ npm run start
```
